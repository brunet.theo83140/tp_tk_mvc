#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
view with a gui for the annuaire
"""
from tkinter import Entry, Label, StringVar, Tk, Button, messagebox

class AnnuaireGui:
    """
    View with a gui
    use Tkinter
    """
    def __init__(self, controller: object):
        self.control = controller
        self.ids = ["name", "last_name", "phone", "adress", "city"]
        self.button_list = ["chercher", "inserer", "effacer"]
        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}

    def create_main_frame(self):
        """
        create the main window
        """
        root = Tk()
        root.title('Annuaire')
        i, j = 0, 0
        for idi in self.ids:
            lab = Label(root, text=idi)
            self.widgets_labs[idi] = lab
            lab.grid(row=i, column=0)
            var = StringVar()
            entry = Entry(root, text=var)
            self.widgets_entry[idi] = entry
            entry.grid(row=i, column=1)

            i += 1

        for idi in self.button_list:
            button = Button(root, text=idi)
            self.widgets_button[idi] = button
            button.grid(row=i + 1, column=j)

            j += 1

        self.widgets_button['inserer'].config(command=lambda: self.press_button('inserer'))
        self.widgets_button['effacer'].config(command=lambda: self.press_button('effacer'))
        self.widgets_button['chercher'].config(command=lambda: self.press_button('chercher'))


        root.mainloop()


    def press_button(self, ident):
        """
        call the operator for the differents button (ident is the name of the button)
        """
        dico_data = {}
        for key in self.widgets_entry:
            dico_data[key] = self.widgets_entry[key].get()
        if ident == 'inserer':
            self.control.insert(dico_data)
            messagebox.showinfo(message='Done')
        elif ident == 'effacer':
            messagebox.askokcancel(message='are you sure?')
            self.control.delete_data(dico_data)
            messagebox.showinfo(message='Done')

        elif ident == 'chercher':
            self.control.search_data(dico_data)

    def display_result(self, list_search: list):
        """
        display the result of the research in a new window
        """
        root = Tk()
        row_number = 1
        column_number = 0
        for i in self.ids:
            label = Label(root, text=i, bd=1, relief='solid', bg='green')
            label.grid(row=0, column=column_number, sticky='nswe')
            column_number += 1

        for individue in list_search:
            list_value = str(individue).split('\t')
            column_number = 0
            for value in list_value:
                label = Label(root, text=value.replace('\n', ''), bd=1, relief='solid')
                label.grid(row=row_number, column=column_number, sticky='nswe')
                column_number += 1
            row_number += 1

    def already_in_database(self):
        """
        create a warning box if the individu is already in the database
        """
        messagebox.showwarning(message='the data are in the database')
