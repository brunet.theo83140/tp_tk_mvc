from tkinter import Entry, Tk, Label, StringVar, END


class Autocomplete_entry(Entry):
    def __init__(self):
        super().__init__()
        self.liste_complete = []
        self.bind('<KeyRelease>', self.autocomplete)
        self.pos = 0
        self.bind('<Tab>',self.validate)

    def validate(self,osef):
        self.pos = len(self.get())
        self.icursor(self.pos)

    def autocomplete(self,dafuck):
        entry_text = self.get()
        self.pos += 1
        for i in self.list_complete:
            ind = self.pos
            if i[0:self.pos] == entry_text[0:self.pos]:
                self.delete(0,END)
                self.insert(0,i)
                break
            self.delete(ind, END)
        self.icursor(self.pos)

    def set_list(self,liste):
        self.list_complete = sorted(liste)
        self.list_complete
if __name__ == '__main__':
    main = Tk()
    liste_com = ['azertyuiop','azertgfddccx',"sfdsfsdf",'fzdfijzoifz','djbpidguigfq','fvudpcdcuzbc']
    entry_test = autocomplete_entry()
    entry_test.set_list(liste_com)
    entry_test.pack()
    main.mainloop()