#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CLI
"""

class Cli:
    """
    CLI to interact with the phonebook
    """
    def __init__(self, controller: object, args: dict):
        self.query = {}
        self.query['name'] = args['name']
        self.query['last_name'] = args['last_name']
        self.query['phone'] = args['phone']
        self.query['adress'] = args['adress']
        self.query['city'] = args['city']
        self.controller = controller
        self.args = args
    def active(self):
        """
        call the function of the controller
        """
        if self.args['action'] == 's':
            self.controller.search_data(self.query)
        elif self.args['action'] == 'i':
            self.controller.insert(self.query)
        elif self.args['action'] == 'd':
            self.controller.delete_data(self.query)

    def already_in_database(self):
        """
        print an error if the query is in the file
        """
        print('########### '+ self.query['name'] + ' Already in database ###########')

    def display_result(self, list_search: list):
        """
        print the result of the search function
        """
        print('query: ' + self.query)
        print(str(len(list_search)) + ' result:')
        for i in list_search:
            print(str(i))
