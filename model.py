"""
Class for Individu and Annuaire
"""
from os.path import exists

class Individu:
    """
    Individu, contain the data
    the two method are for the display (str) and the equal (eq)
    """
    def __init__(self, name='', last_name='', phone='', adress='', city=''):
        self.name = name
        self.last_name = last_name
        self.phone = phone
        self.adress = adress
        self.city = city


    def __eq__(self, other):

        if other.name != '':
            if other.name != self.name:
                return False
        if other.last_name != '':
            if other.last_name != self.last_name:
                return False
        if other.phone != '':
            if other.phone != self.phone:
                return False
        if other.adress != '':
            if other.adress != self.adress:
                return False
        if other.city != '':
            if other.city != self.city:
                return False
        return True

    def __str__(self):
        liste_value = list(self.__dict__.values())
        return '\t'.join(liste_value)+'\n'

class Annuaire:
    """
    Annuaire
    take a tsv file who contain 1 individu per row
    """
    def __init__(self, path: str):
        self.data = []
        self.path = path
        if exists(path):
            file = open(path, 'r')
            for i in file:
                tmp = i.split('\t')
                individu = Individu(tmp[0],
                                    tmp[1],
                                    tmp[2],
                                    tmp[3],
                                    tmp[4].replace('\n', ''))
                self.data.append(individu)
            del self.data[0]
        else:
            file = open(self.path, 'w')
            file.write('name\tlast_name\tphone\tadress\tcity\n')
        file.close()

    def new_individu(self, individu: object):
        """
        insert a new individu in the file
        """
        file = open(self.path, 'a')
        file.write(str(individu))
        self.data.append(individu)
        file.close()

    def is_in(self, query):
        """
        return the individu if individu == query
        """
        list_equal = []
        for i in self.data:
            if i == query:
                list_equal.append(i)
        return list_equal

    def delete_individu(self, query):
        """
        remove the individu if individue == query
        """
        compteur = 0
        del_list = []
        for i in self.data:
            if i == query:
                del_list.append(compteur)
            compteur += 1
        compteur = 0
        for i in del_list:
            del self.data[i-compteur]
            compteur += 1

        file = open(self.path, 'w')
        file.write('name\tlast_name\tphone\tadress\tcity\n')
        for i in self.data:
            file.write(str(i))
        file.close()
