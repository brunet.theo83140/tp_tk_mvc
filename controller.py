"""
controller of the annuaire
"""
from view import AnnuaireGui
from model import Individu
from model import Annuaire

class Controller:
    """
    controller, interact with model (individue and annuaire)
    and view (gui or cli)
    """
    def __init__(self, path: str):
        self.database = Annuaire(path)
        self.view = None

    def gui(self):
        """
        create the main window of the gui
        input: None
        output: None
        """
        self.view = AnnuaireGui(self)
        self.view.create_main_frame()

    def cli(self):
        pass

    def insert(self, data: dict):
        """
        verif if the individue is in the file (is_in) and insert into the file
        (new_individue)
        input: dict
        output: None
        """
        query = self.dict_to_individue(data)
        if self.database.is_in(query) == []:
            self.database.new_individu(query)
        else:
            self.view.already_in_database()

    def delete_data(self, data: dict):
        """
        call the delete_individue method of annuaire
        input: dict
        output: None
        """
        query = self.dict_to_individue(data)
        self.database.delete_individu(query)

    def search_data(self, data: dict):
        """
        search if the data exist in the file and return a list
        with all the individue
        input: dict
        output: list
        """
        query = self.dict_to_individue(data)
        result_list = self.database.is_in(query)
        self.view.display_result(result_list)

    @staticmethod
    def dict_to_individue(data: dict):
        """
        transform a dict
        input: dict
        output: object Individu
        """
        individu = Individu(data['name'],
                            data['last_name'],
                            data['phone'],
                            data['adress'],
                            data['city'])
        return individu
